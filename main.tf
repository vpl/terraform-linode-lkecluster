terraform {
  required_providers {
    linode = {
      source  = "linode/linode"
      version = ">= 1.12.4"
    }
  }
}

# provider "linode" {
#   token = "$LINODE_TOKEN"
# }

resource "linode_lke_cluster" "cluster" {
  label       = var.label
  k8s_version = var.k8s_version
  region      = var.region
  tags        = var.tags

  dynamic "pool" {
    for_each = var.pools
    content {
      type  = pool.value["type"]
      count = pool.value["count"]
    }
  }
}

locals {
  kubeconfig = yamldecode(base64decode(linode_lke_cluster.cluster.kubeconfig))
}
