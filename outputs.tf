output "kubeconfig" {
  value = local.kubeconfig
}

output "token" {
  value = local.kubeconfig.users[0].user.token
}

output "host" {
  value = local.kubeconfig.clusters[0].cluster.server
}

output "ca_certificate" {
  value = base64decode(local.kubeconfig.clusters[0].cluster["certificate-authority-data"])
}
